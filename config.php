<?php
error_reporting(E_ALL);

define('ROOT_PATH', dirname(__FILE__));
define('UPLOAD_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . "uploads");
define('FILE_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . "files");
define('CLASS_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . "class");
define('ENCRYPT_KEY', file_get_contents(ROOT_PATH . DIRECTORY_SEPARATOR . "key.txt"));

define('DB_HOST', 'localhost');
define('DB_NAME', 'test');
define('DB_USER', 'db_user');
define('DB_PASS', '111');
define('DB_CHARSET', 'utf8');

require_once ROOT_PATH . DIRECTORY_SEPARATOR . "functions.php";

session_start();

$dsn = "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=" . DB_CHARSET . "";
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
];
$pdo = new PDO($dsn, DB_USER, DB_PASS, $opt);

$user = checkRememberMe($pdo, $_SESSION['user'] ?? [], $_COOKIE['id'] ?? 0);
if ($user) {
    $_SESSION['user'] = $user;
}