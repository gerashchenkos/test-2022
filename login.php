<?php

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "config.php";
require_once CLASS_PATH . DIRECTORY_SEPARATOR . "User.php";

if (!empty($_SESSION['user'])) {
    header("Location: /profile.php");
    die();
}

if (!empty($_POST['email']) && !empty($_POST['psw'])) {

    //doing login check here
    /*$users = json_decode(
        file_get_contents(FILE_PATH . DIRECTORY_SEPARATOR . "users.json"),
        true
    );
    $postData = $_POST;
    //$user = array_search($_POST['email'], array_column($users, 'email'));
    $user = array_filter(
        $users,
        function ($user) use ($postData) {
            if ($postData['email'] === $user['email'] && $postData['psw'] === $user['password']) {
                return $user;
            }
        }
    )[0];
    unset($user['password']);*/

    $email = $_POST['email'];
    $password = $_POST['psw'];
    //$stmt = $pdo->prepare("SELECT * FROM users WHERE `email` = :email and `password` = :password");
    //$stmt->execute(["email" => $email, "password" => $password]);
    //$user = $stmt->fetch(PDO::FETCH_ASSOC);
    $user = User::login($pdo, $email, $password);
    if (!empty($user)) {
        //$userObj = new User($user['id'], $user['name'], $user['email'], $user['password'], $user['phone'] ?? null, $user['status']);
        $_SESSION['user'] = serialize($user);
        if (!empty($_POST['remember'])) {
            setcookie("id", safeEncrypt($user->id, ENCRYPT_KEY), time() + (3600 * 24 * 30) );
        }
        header("Location: /profile.php");
    } else {
        $isError = 1;
    }
}

require_once ROOT_PATH . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR . "login.php";
