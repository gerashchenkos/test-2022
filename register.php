<?php

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "config.php";
require_once CLASS_PATH . DIRECTORY_SEPARATOR . "User.php";

if (!empty($_SESSION['user'])) {
    header("Location: /profile.php");
    die();
}

$error = [];
$notEmpty = ['name', 'email', 'password', 'confirm_password'];
if (!empty($_POST)) {
    foreach ($_POST as $key => $val) {
        if (in_array($key, $notEmpty) && empty($val)) {
            $error[$key] = ucfirst($key) . ' is required';
        }

        if ($key === "email") {
            if (!filter_var($val, FILTER_VALIDATE_EMAIL)) {
                $error[$key] = ucfirst($key) . ' is invalid';
                continue;
            }

            $stmt = $pdo->prepare("SELECT id FROM users WHERE `email` = :email");
            $stmt->execute(["email" => $val]);
            if (!empty($stmt->fetchColumn())) {
                $error[$key] = ucfirst($key) . " " . $val . ' is already used';
            }
        }

        if ($key === "phone" && !empty($val)) {
            if (!preg_match("/^[0-9]{3}-[0-9]{4}-[0-9]{4}$/", $val)) {
                $error[$key] = ucfirst($key) . ' is invalid';
            }
        }
    }

    if ($_POST['password'] !== $_POST['confirm_password']) {
        $error['password'] = 'Password was not confirmed';
    }

    if (empty($error)) {
        /*$stmt = $pdo->prepare("INSERT INTO `users`(`name`, `email`, `password`, `phone`) VALUES(:name, :email, :password, :phone)");
        $user = [
            "name" => $_POST['name'],
            "email" => $_POST['email'],
            "password" => $_POST['password'],
            "phone" => $_POST['phone'],
        ];
        $stmt->execute($user);
        $id = $pdo->lastInsertId();*/
        $user = User::create($pdo, $_POST);
        //$user['id'] = $user->id;
        $_SESSION['user'] = serialize($user);
        header("Location: /profile.php");
    }
}

require_once ROOT_PATH . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR . "register.php";
