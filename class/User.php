<?php

class User
{
    public int $id;
    public string $name;
    public string $email;
    private string $password;
    public string $phone;
    private string $status;
    private const SALT = "edje&x$11_cVB";
    private const STATUSES = ['not activated', 'active', 'disabled'];

    public function __construct(
        int $id,
        string $name,
        string $email,
        string $password,
        string $phone = null,
        string $status = 'not activated'
    ) {
        $this->id = (int)$id;
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        if (!empty($phone)) {
            $this->phone = $phone;
        }
        $this->setStatus($status);
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $this->encryptPassword($password);
    }

    private function encryptPassword(string $password): string
    {
        return crypt($password, self::SALT);
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        if (in_array($status, self::STATUSES)) {
            $this->status = $status;
        } else {
            throw new Exception('Status is invalid');
        }
    }

    public function __destruct()
    {
        //var_dump("class is destroyed");
    }

    public function __call($name, $arguments)
    {
        // Замечание: значение $name регистрозависимо.
        $nameParams = explode("get", $name);
        if (in_array(strtolower($nameParams[1]), ["id", "name", "email", "phone"])) {
            return $this->{strtolower($nameParams[1])};
        }
    }

    public function __get(string $name)
    {
        if (in_array($name, ["status"])) {
            return $this->status;
        }
    }

    public function __set(string $name, mixed $value): void
    {

    }

    public function __toString(): string
    {
        return $this->id . " " . $this->name . " " . $this->email;
    }

    public function __serialize(): array
    {
        return ["id" => $this->id, "name" => $this->name, "email" => $this->email];
    }

    public static function create(object $db, array $userData): User
    {
        $stmt = $db->prepare("INSERT INTO `users`(`name`, `email`, `password`, `phone`) VALUES(:name, :email, :password, :phone)");
        $user = [
            "name" => $userData['name'],
            "email" => $userData['email'],
            "password" => $userData['password'],
            "phone" => $userData['phone'],
        ];
        $stmt->execute($user);
        $id = $db->lastInsertId();
        $user = new User($id, $userData['name'], $userData['email'], $userData['password'], $userData['phone']);
        return $user;
    }

    public static function login(object $db, string $email, string $password): User|bool
    {
        $stmt = $db->prepare("SELECT * FROM users WHERE `email` = :email and `password` = :password");
        $stmt->execute(["email" => $email, "password" => $password]);
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!empty($user)) {
            $user = new User($user['id'], $user['name'], $user['email'], $user['password'], $user['phone'] ?? null, $user['status']);
            return $user;
        }
        return false;
    }

}