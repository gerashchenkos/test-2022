<?php

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "config.php";
require_once CLASS_PATH . DIRECTORY_SEPARATOR . "User.php";

if (empty($_SESSION['user'])) {
    header("Location: /login.php");
    die();
}

if (!empty($_POST['name']) && !empty($_FILES)) {
    if (!move_uploaded_file(
        $_FILES['photo']['tmp_name'],
        UPLOAD_PATH . DIRECTORY_SEPARATOR . basename(
            $_POST['name'] . "." . pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION)
        )
    )) {
        die("FILE_UPLOAD_ERROR");
    }
}
$a = [];

require_once ROOT_PATH . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR . "profile.php";