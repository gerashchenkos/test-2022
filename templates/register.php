<?php require_once("header.php"); ?>


<div class="container-fluid">
    <div class="row justify-content-md-center">
        <div class="col-6">
            <form action="/register.php" method="post">
                <div class="container">

                    <div class="mb-3">
                        <label for="email" class="form-label">Name</label>
                        <input class="form-control <?php if(!empty($error['name'])): ?>is-invalid<?php endif; ?>" type="text" placeholder="Enter Name" name="name" value="<?php echo $_POST['name'] ?? ''; ?>" required>
                        <div class="invalid-feedback"><?php echo $error['name'];?></div>
                    </div>

                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input class="form-control <?php if(!empty($error['email'])): ?>is-invalid<?php endif; ?>" type="email" placeholder="Enter Email" name="email" value="<?php echo $_POST['email'] ?? ''; ?>" required>
                        <div class="invalid-feedback"><?php echo $error['email'];?></div>
                    </div>

                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" class="form-control <?php if(!empty($error['password'])): ?>is-invalid<?php endif; ?>" id="password" placeholder="Enter Password" name="password"
                               required>
                        <div class="invalid-feedback"><?php echo $error['password'];?></div>
                    </div>

                    <div class="mb-3">
                        <label for="confirm-password" class="form-label">Confirm Password</label>
                        <input type="password" class="form-control <?php if(!empty($error['confirm_password'])): ?>is-invalid<?php endif; ?>" id="confirm-password" placeholder="Enter Password" name="confirm_password"
                               required>
                        <div class="invalid-feedback"><?php echo $error['confirm_password'];?></div>
                    </div>

                    <div class="mb-3">
                        <label for="phone" class="form-label">Phone</label>
                        <input class="form-control <?php if(!empty($error['phone'])): ?>is-invalid<?php endif; ?>" type="text" placeholder="Enter Phone" name="phone" value="<?php echo $_POST['phone'] ?? ''; ?>">
                        <div class="invalid-feedback"><?php echo $error['phone'];?></div>
                    </div>

                    <div class="col-3">
                        <button type="submit" class="btn btn-primary">Register</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<!--<form action="/register.php" method="post">
    <div class="container">
        <label for="uname"><b>Name</b></label>
        <input type="text" placeholder="Enter Name" name="" value="<?php echo $_POST['name'] ?? ''; ?>" required>
        <?php if(!empty($error['name'])): ?>
            <p class="error"><?php echo $error['name'];?></p>
        <?php endif; ?>

        <label for="email"><b>Email</b></label>
        <input type="email" placeholder="Enter Email" name="email" value="<?php echo $_POST['email'] ?? ''; ?>" required>
        <?php if(!empty($error['email'])): ?>
            <p class="error"><?php echo $error['email'];?></p>
        <?php endif; ?>

        <label for="psw"><b>Password</b></label>
        <input type="password" placeholder="Enter Password" name="password" required>
        <?php if(!empty($error['password'])): ?>
            <p class="error"><?php echo $error['password'];?></p>
        <?php endif; ?>

        <label for="psw"><b>Confirm Password</b></label>
        <input type="password" placeholder="Enter Password" name="confirm_password" required>
        <?php if(!empty($error['confirm_password'])): ?>
            <p class="error"><?php echo $error['confirm_password'];?></p>
        <?php endif; ?>

        <label for="uname"><b>Phone</b></label>
        <input type="text" placeholder="Enter Phone" name="phone" value="<?php echo $_POST['phone'] ?? ''; ?>">
        <?php if(!empty($error['phone'])): ?>
            <p class="error"><?php echo $error['phone'];?></p>
        <?php endif; ?>

        <button type="submit">Register</button>
    </div>
</form>-->
</body>
</html>