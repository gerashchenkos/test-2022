<?php
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "config.php";

require_once CLASS_PATH . DIRECTORY_SEPARATOR . "User.php";

if (empty($_SESSION['user'])) {
    header("Location: /login.php");
    die();
}

require_once ROOT_PATH . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR . "index.php";